package HRFileExplorer_webcam;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class GrayScale {

    //BufferedImage image;
    int width;
    int height;
    int count = 0;
    long sum = 0;

    @SuppressWarnings("empty-statement")
    public GrayScale(BufferedImage image) {

        try {
            //File input = new File("C:\\Users\\Ayonij\\Documents\\NetBeansProjects\\projectform\\output.jpg");
//            File input = new File("C:\\Users\\Ayonij\\Desktop\\Untitled.jpg");
//             image = ImageIO.read(input);
            width = image.getWidth();
            height = image.getHeight();
            int i = 0, j = 0;
            float x[][] = new float[height][width];
            float z[][] = new float[height][width];
            double a[][] = new double[height][width];
            double w[][] = new double[3][3];
            double s[][] = new double[3][3];

//          w[0][0] =  0.0;
//             System.out.println(1 / 16.0);
//            w[0][1] =  -1.0;
//            w[0][2] =  0.0;
//            w[1][0] =  -1.0;
//            w[1][1] =  5.0;
//            w[1][2] =  -1.0;
//            w[2][0] =  0.0;
//            w[2][1] =  -1.0;
//            w[2][2] =  0.0;
//Scaling Matrix definition
            s[0][0] = -1 / 9.0;
            //    System.out.println(1 / 16.0);
            s[0][1] = -1 / 9.0;
            s[0][2] = -1 / 9.0;
            s[1][0] = -1 / 9.0;
            s[1][1] = 8 / 9.0;
            s[1][2] = -1 / 9.0;
            s[2][0] = -1 / 9.0;
            s[2][1] = -1 / 9.0;
            s[2][2] = -1 / 9.0;
            for (int c = 0; c < 3; c++) {
                for (int y = 0; y < 3; y++) {
                    w[c][y] = 1 / 9.0;
                }
            }
//Greyscale conversion
            for (i = 0; i < height; i++) {
                for (j = 0; j < width; j++) {
                    Color c = new Color(image.getRGB(j, i));
//                    int red = (int)(c.getRed() * 0.299);
//               int green = (int)(c.getGreen() * 0.587);
//               int blue = (int)(c.getBlue() *0.114);

                    int red = (int) (c.getRed());
                    int green = (int) (c.getGreen());
                    int blue = (int) (c.getBlue());
//                    Color newColor = new Color((int) (red + green + blue) / 3, (int) (red + green + blue) / 3, (int) (red + green + blue) / 3);
                    x[i][j] = (red + green + blue) / 3;
//                    image.setRGB(j, i, newColor.getRGB());
                    count++;

                }
            }
//            File ouptut = new File("grayscale.jpg");
//            ImageIO.write(image, "jpg", ouptut);

//Smoothing filter
            for (i = 1; i < height - 1; i++) {
                for (j = 1; j < width - 1; j++) {
                    z[i][j] = 0;
                    for (int p = -1; p < 2; p++) {
                        for (int q = -1; q < 2; q++) {
                            z[i][j] += x[i + p][j + q] * w[p + 1][q + 1];
                            if (z[i][j] > 255) {
                                z[i][j] = 255;
                            } else if (z[i][j] < 0) {
                                z[i][j] = Math.abs(z[i][j]);
                            }
                            //  System.out.println(z[i][j]);
                        }
                    }
//                    Color newColor1 = new Color((int) z[i][j], (int) z[i][j], (int) z[i][j]);
//                    image.setRGB(j, i, newColor1.getRGB());
                }
            }
//            File ouptut1 = new File("grayscale1.jpg");
//            ImageIO.write(image, "jpg", ouptut1);

//Sharpening filter
            for (i = 1; i < height - 1; i++) {
                for (j = 1; j < width - 1; j++) {
                    a[i][j] = 0;
                    for (int p = -1; p < 2; p++) {
                        for (int q = -1; q < 2; q++) {
                            a[i][j] += z[i + p][j + q] * s[p + 1][q + 1];
                            if (a[i][j] > 255) {
                                a[i][j] = 255;
                            } else if (a[i][j] < 0) {
                                a[i][j] = Math.abs(a[i][j]);
                            }
                            //System.out.println(a[i][j]);
                        }
                    }
                    Color newColor2 = new Color((int) a[i][j], (int) a[i][j], (int) a[i][j]);
                    image.setRGB(j, i, newColor2.getRGB());
                    sum += a[i][j];
                }
            }
            float avg_thresh = sum / count;
            //File output_img = new File("output.jpg");
            //ImageIO.write(image,"jpg",output_img);
//Thresholding            
//            for (i = 1; i < height - 1; i++) {
//                for (j = 1; j < width - 1; j++) {
//                    if (a[i][j] >= avg_thresh) {
//                        a[i][j] = 0;
//                    } else {
//                        a[i][j] = 255;
//                    }
//                    Color newColor = new Color((int) a[i][j], (int) a[i][j], (int) a[i][j]);
//                    image.setRGB(j, i, newColor.getRGB());
//                    
//                }
//            }

//            File output_img = new File("output.jpg");
//            ImageIO.write(image,"jpg",output_img);
//            ouptut2 = new File("grayscale2.jpg");
//            ImageIO.write(image, "jpg", ouptut2);
            //System.out.println(avg_thresh);
            
        } catch (Exception e) {
            System.out.println(e);
        }

//    static public void main(String args[]) throws Exception {
//   //     GrayScale obj = new GrayScale();
//    }
    }
}
