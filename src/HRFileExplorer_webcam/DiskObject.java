package HRFileExplorer_webcam;

public class DiskObject {
    public static String TYPE_COMPUTER = "Computer";
    public static String TYPE_DRIVER = "Driver";
    public static String TYPE_FOLDER = "Folder";
    public static String TYPE_FILE = "File";

    public String name;
    public String type;

    public DiskObject(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String toString() {
        return name;
    }
}