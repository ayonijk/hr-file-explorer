package HRFileExplorer_webcam;

import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.table.DefaultTableCellRenderer;

public class MyTableRenderer extends DefaultTableCellRenderer {
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        if (value != null && value instanceof DiskObject) {
            // Set the Name column as left aligned.
            DiskObject diskObject = (DiskObject) value;

            ((JLabel) component).setText(diskObject.name);
            ((JLabel) component).setHorizontalAlignment(JLabel.LEFT);

            if (diskObject.type.equals(DiskObject.TYPE_DRIVER)) {
                ((JLabel) component).setIcon(FileExplorer.driverIcon);
            } else if (diskObject.type.equals(DiskObject.TYPE_FOLDER)) {
                ((JLabel) component).setIcon(FileExplorer.folderIcon);
            } else {
                ((JLabel) component).setIcon(FileExplorer.fileIcon);
            }

            return component;
        } else if (value != null && value instanceof String) {
            // Set the Size column as right aligned.
            ((JLabel) component).setHorizontalAlignment(JLabel.RIGHT);

            return component;
        }

        return null;
    }
}
