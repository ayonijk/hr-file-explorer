package HRFileExplorer_webcam;

import com.googlecode.javacv.cpp.opencv_core;
import com.googlecode.javacv.cpp.opencv_highgui;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;

public class NJF2 extends JFrame {

    JLabel jlbl1 = new JLabel();
    JPanel jp1 = new JPanel();

    public NJF2() {
        setLayout(new BorderLayout());
        add(jp1, BorderLayout.CENTER);
        jp1.add(jlbl1);
        jlbl1.setLayout(new FlowLayout());
        setSize(600, 400);
        show();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    //open webcam
    public void opcam() {
        Thread webcam;
        webcam = new Thread() {
            //@Override
            @Override
            public void run() {
                opencv_highgui.CvCapture capture = opencv_highgui.cvCreateCameraCapture(0);
                opencv_highgui.cvSetCaptureProperty(capture, opencv_highgui.CV_CAP_PROP_FRAME_HEIGHT, 400);
                opencv_highgui.cvSetCaptureProperty(capture, opencv_highgui.CV_CAP_PROP_FRAME_WIDTH, 600);
                opencv_core.IplImage grabbedimage;
                //GrayScale gs;
                opencvtempl_matching optm = null;
                grabbedimage = opencv_highgui.cvQueryFrame(capture);
                //NJF2 njf=new NJF2();

                while ((grabbedimage = opencv_highgui.cvQueryFrame(capture)) != null) {
                    try {
                        BufferedImage b = grabbedimage.getBufferedImage();
                        File ouptut = new File("output.jpg");
                        ImageIO.write(b, "jpg", ouptut);
                        grabbedimage = cvLoadImage("C:\\Users\\Ayonij\\Documents\\Major Project\\webcapp\\output.jpg", 0);
                        //System.out.println("1");
//                        gs=new GrayScale(b);
//                        File ouptut1 = new File("Mousemove.jpg");
//                        ImageIO.write(b, "jpg", ouptut1);
                        //System.exit(0);
                        optm = new opencvtempl_matching(grabbedimage);
                        //System.out.println("2");
                        //njf.jlbl1.setIcon(b);
                       // Thread.sleep(500);

                    } catch (Exception ex) {
                        Logger.getLogger(NJF2.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        };
        webcam.start();
    }

    public static void main(String args[]) {
        NJF2 n = new NJF2();
        n.opcam();
    }
}
