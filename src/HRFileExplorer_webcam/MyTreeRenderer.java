package HRFileExplorer_webcam;

import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;
import javax.swing.JTree;
import java.io.File;

public class MyTreeRenderer extends DefaultTreeCellRenderer {
    public Component getTreeCellRendererComponent(
            JTree tree, Object value, boolean isSelected, boolean isExpanded,
            boolean leaf, int row, boolean hasFocus) {
        Component component = super.getTreeCellRendererComponent(tree, value,
                isSelected, isExpanded, leaf, row, hasFocus);

        if (value != null && value instanceof MyTreeNode) {
            MyTreeNode treeNode = (MyTreeNode) value;
            // ===
            // For Windows "My Computer" node only.
            // ===
            File selectedDir = (File) treeNode.getUserObject();

            if (selectedDir.equals(new File(FileExplorer.MY_COMPUTER_FOLDER_PATH))) {
                setIcon(FileExplorer.computerIcon);
            } else if (selectedDir.getParent() == null) {
                setIcon(FileExplorer.driverIcon);
            } else {
                setIcon(FileExplorer.folderIcon);
            }

            return component;
        }

        return this;
    }
}
