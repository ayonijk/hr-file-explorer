package HRFileExplorer_webcam;

import javax.swing.*;
import java.awt.*;

class MyStatusBar extends Box {
    public JLabel lblObject, lblSize, lblDesc;

    public MyStatusBar() {
        super(BoxLayout.X_AXIS);

        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();

        // Add the JLabel displaying the selected object numbers.
        lblObject = new JLabel("Selected Object(s):", SwingConstants.LEADING);
        lblObject.setPreferredSize(new Dimension((int) (0.6 * screenSize.width),
                22));
        lblObject.setBorder(BorderFactory.createLoweredBevelBorder());
        this.add(lblObject, null);

        // Add the JLabel displaying the selected object size.
        lblSize = new JLabel("Size:", SwingConstants.LEADING);
        lblSize.setPreferredSize(new Dimension((int) (0.2 * screenSize.width),
                22));
        lblSize.setBorder(BorderFactory.createLoweredBevelBorder());
        this.add(lblSize, null);

        // Add the JLabel displaying the description.
        lblDesc = new JLabel("Description:", SwingConstants.LEADING);
        lblDesc.setPreferredSize(new Dimension((int) (0.2 * screenSize.width),
                22));
        lblDesc.setBorder(BorderFactory.createLoweredBevelBorder());
        this.add(lblDesc, null);
    }
}